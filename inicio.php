<?php

  	session_start();

  	if( isset($_SESSION['id']) && isset($_SESSION['user']) && isset($_SESSION['rol']) ) {

        echo '<a href="logout.php">Cerrar Sesión</a>';


        if ($_SESSION['rol'] == 1) {
            include 'administrador.php';

        }elseif ($_SESSION['rol'] == 2){
            include 'cliente.php';
            
        }else{
            echo '<script> window.location="index.php"; </script>';
        }

    }else{
        echo '<script> window.location="index.php"; </script>';
    }

?>