<?php 

require_once __DIR__ . '/../../config/vars.php'; // requerimos el archivo de variables que hemos creado

/*
* Clase Padre conexion y hereda datos Variables
*/
class Conexion extends Variables{ 
    
    // funcion publica en el cual lo que haremos será conectar a nuestra Db
    public function ConectarSesion(){
        $connect = mysqli_connect($this->host,$this->user,$this->pass,$this->db_sesion) // al final de la linea es donde llamamos a la variable que hemos creado en vars.php
            or die ("Error". mysqli_error($connect));
        return $connect;   
    }
    
    // y ejecutamos la conecion... el ejecutarPruebas es el que utilizaremos para cuando hagamos nuestras consultas sql en el principalControllers
    public function EjecutarSesion($sql){
        $connect = $this->ConectarSesion(); //llamamos a la funcion que hemos creado de conectar a nuestra DB
        $result = $connect->query($sql);
        return $result;
    }
    
   
}



?>