function session() {
    var usuario = $('#usuario').val();
    var clave = $('#clave').val();

    var param ={'Funcion':'session_user', 'usuario':usuario, 'clave':clave };
    $.ajax({
        data: JSON.stringify (param),
        type:"JSON",
        url: 'ajax.php',
        success:function(data){
            var json = JSON.parse(data);
            var a = json[0].detalle;
            if (a > 0 ) {
                var page = json[0].page;
                setTimeout(function(){
                    window.location=page;
                },500);
            }else{
                $('#alert').html(`
                
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  Usuario o clave incorrectos
                </button>
                
                `);
            }
        }
    });
}